package ru.inordic.in3031.gradle;

public class Element {
    public int value;
    public Element next;
    public Element previous;

    public Element(int value) {
        this.value = value;
    }
}